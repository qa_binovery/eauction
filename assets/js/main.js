$(document).ready(function(){
    var keypair = StellarBase.Keypair.random();
    var apiServer = "http://5.9.6.118:8001"
    var walletServer = "http://5.9.6.118:3000"
    var client = new StellarBase.StellarClient(apiServer, keypair);
    //Switching between PURCHASE AND SALE and RENT, SF, EM on the create-lot page
    $('.contract-terms-select').change( function () {
        switch( $(this).val() ) {
            case '0':
                $('.create-form').removeClass('rent');
                break;
            case '1':
                $('.create-form').addClass('rent');
                break;
        };
    });
	console.log(StellarBase.xdr);
	
	//console.log(new Date());
	
    //Tabs between Description lot and Specification lot
    $('.descript-btn').click(function() {
        $('.specif-btn').removeClass('active');
        $(this).addClass('active');
        $('.specification-text').removeClass('active');
        $('.description-text').addClass('active');
    });
    $('.specif-btn').click(function() {
        $('.descript-btn').removeClass('active');
        $(this).addClass('active');
        $('.description-text').removeClass('active');
        $('.specification-text').addClass('active');
    });
    
    
    $(document).on('click', '#register', function(){
    	
		var name = $("#username").val();
		var pass = $("#password").val();
		var dom = $("#domain").val();
		
		StellarBase.EncryptedWalletStorage.registerWallet(walletServer, keypair, name, pass, dom).then(function (walletServerResponse) {
			$(".edrpo").show();
			console.log(walletServerResponse);
		});
		
		return false;
	});
	
	
	$(document).on('click', '#postreg', function(){
		var keypair = StellarBase.Keypair.random();
		var accountId = keypair.accountId();
		var codeKYC = $("#edrpo").val(); 
		console.log(accountId);
		console.log(codeKYC);
		var data =
			{'destination': accountId,
		     'code_kyc': codeKYC,
		     'submit': 'register'
		    };
		$.post(apiServer + "/users/register", data).then(function (response) {
			if (response.state==0) {
				$('.register').hide();
				$('.regsuccess').show();
				$('.edrpo').hide();
			}
			console.log(response);
		});
		
		return false;
	})
    
    $(document).on('click', '#login', function(){
    	
    	var name = $("#lusername").val();
		var pass = $("#lpassword").val();
		var domain = $("#ldomain").val();
    	
    	StellarBase.EncryptedWalletStorage.getWallet(walletServer, name, pass, domain).then(function (wallet) {
    		var rawKeychainData = wallet.rawKeychainData;
    		rawKeychainData = JSON.parse(rawKeychainData.replace(new RegExp("%22", 'g'), '"'));
    		
    		$.ajax({
			  type: "POST",
			  url: '/login/auth',
			  data: {'username':wallet.username, 'walletId':wallet.walletId, 'walletKey':wallet.walletKey, 'accountId':rawKeychainData.accountId, 'masterSeed':rawKeychainData.masterSeed},
			  dataType: 'json',
			  success: function(json){
			  	console.log(json);
				if (json.logged) {
					location = '/';
					location.reload();
				}
			  },
			  error: function(xhr, ajaxOptions, thrownError) {
				console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			  }
			  
			});
    		
    	});
    	
    	return false;
    	
    });
    
    $(document).on('click', '#createlot', function(){
    	var formdata = $("#lotform input, #lotform select, #lotform textarea");
    	//console.log(data);
    	//EncryptedWalletStorage.getWallet(walletServer, login, password, domain)
    	
    	$.getJSON(apiServer+'/application/node_public_key', '', function (npk) {
    		var platformId = npk.public_key;
    		var url = '/lot/addlot/'+platformId;
    		//console.log(url);
    		//var today = new Date();
    		+new Date;
    		
			var lot = {
				accountId: $("#accountId").val(),
			    lotId: "", // опцiональний айдi лоту, може бути згенерованим як StellarBase.Keypair.random().accountId()
			    lot_name: $("#lotname").val(), //назва лота аукціона
			    lot_address: $("#address").val(), //адреса
			    latitude:'46.2818',
			    logitude:'30.4237',
			    lot_location: "{46.2818, 30.4237}", //геолокація
			    type: $("#lottype").val(), //тип об'єкта
			    branch: '', //галузь діяльності, 
			    code: "<кадастровий код>",
			    start_price: $("#stprice").val(), //початкова ціна
			    min_step: $("#steps").val(), //мінімальний крок аукціона, %
			    max_step: $("#steps").val()*10, //максимальний крок аукціона, %
			    duration: 120, //тривалість аукціона, хвилини
			    publicationTime: Date.now(), //час публікації (час створення об'єкта)
			    auctionStartTime: '', //час початку прийому ставок
			    pledge: "10", //депозитний внесок (забезпечення)
			    pledgeConditions: "Умови поверненя/неповернення забезпечення", //обов'язкове поле, коли забезпечення більше 0
			    contact: $("#author").val(),
			    specification: { //технічна специфікація
			        ref: "https://drive.google.com/laksjdfl/spec.pdf", //посилання на специфікацію на зовнішньому ресурсі
			        hash: "A23423F23B" //контрольна сума файла
			    },
			    financialReport: { //фінансова звітність (для підприємств)
			        ref: "https://drive.google.com/laksjdfl/report.pdf", //посилання на звітність на зовнішньому ресурсі
			        hash: "A23423F245B" //контрольна сума файла
			    },
			    additionalInfo: { //додаткова інформація (документ)
			        ref: "https://drive.google.com/laksjdfl/info.pdf", //посилання на інфо на зовнішньому ресурсі
			        hash: "A23423F23B" //контрольна сума файла
			    },
			    auctionPlatformId: platformId //ідентифікатор майданчика 
			}
    		var stellar_lot_entry=lot;
    		$.post(apiServer + "/lots/create", stellar_lot_entry).then(function (response) {
			
			console.log(response);
		});
    		
    		
    		client.createLot(lot, function (response) {
			    console.log(response);
			});
    		
    		// $.ajax({
			  // type: "post",
			  // url: url,
			  // data: formdata,
			  // dataType: 'json',
			  // success: function(json){
			  	// console.log(formdata);
			  	// console.log(json);
// 			  	
			  // },
			  // error: function(xhr, ajaxOptions, thrownError) {
				// console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			  // }
// 			  
			// });
    	});
    	
    	return false;
    });
    
    
    
    if ($("#lots-list").length) {
    	
    	$.ajax({
		  type: "GET",
		  url: apiServer+'/lots',
		  data: {},
		  dataType: 'json',
		  success: function(json){
		  	console.log(json);
		  	//console.log(StellarBase.xdr)
		  	
		  	var html = '';
		  	$.each(json, function (k, v) {
		  		
			  	html += '<li class="lot">';
		        html += '<a href="/lot/view/'+v.lotid+'">';
		        html += '<h2>&laquo;'+v.name+'&raquo;</h2>';
		        //html += '<img alt="'+v.name+'" src="'+v.specification.ref+'" />';
		        html += '<p>Ідентифікаційний код 12345678<br>, 07400, Київська обл., м. Київ, вул. Грушевського, 100.<br> Відокремлені підрозділі та предствавництва відсутні.</p>';
		        html += '</a>';
		        html += '</li>';
			  	//txHandler(v);
			  	
			  	//console.log( JSON.stringify(StellarBase.xdr.LotEntry.fromXDR(v.location, 'base64')) );
			  	
		  	});
		  	$("#lots-list").html(html);
		  	$("#loading_lots").hide();
		  	
		  	
		  },
		  error: function(xhr, ajaxOptions, thrownError) {
			console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		  }
		  
		});
    	
    }
    
    if ($("#lot-description").length) {
    	$.ajax({
		  type: "GET",
		  url: apiServer+'/lots/'+$("#lotid").val(),
		  data: {'username':''},
		  dataType: 'json',
		  success: function(json){
		  	console.log(json);
		  	$('h2').text(json.name);
		  	$('.title-line').text(json.name);
		  	$('.address').text(json.address);
		  	$('.plcond').text(json.pledgeConditions);
		  	$('.spec').text(json.specification.ref);
		  	
		  	$('.startprice span').text(json.startprice);
		  	$('.minstep span').text(json.minstep);
		  	$('.maxstep span').text(json.maxstep);
		  	$('.publicationtime span').text(json.publicationtime);
		  	
		  	$("#map iframe").attr('src', 'http://test.svcontact.ru/map.php?lat='+json.latitude+'&lng='+json.longitude+'&z=10');
		  	
		  },
		  error: function(xhr, ajaxOptions, thrownError) {
			console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		  }
		  
		});
    }
    
    function txHandler(txResponse) {
	    console.log( JSON.stringify(StellarBase.xdr.TransactionEnvelope.fromXDR(txResponse.envelope_xdr, 'base64')) );
	    console.log( JSON.stringify(StellarBase.xdr.TransactionResult.fromXDR(txResponse.result_xdr, 'base64')) );
	    console.log( JSON.stringify(StellarBase.xdr.TransactionMeta.fromXDR(txResponse.result_meta_xdr, 'base64')) );
	};
});

