import {hash} from "./index";
import {Keypair} from "./keypair";

export function createParticipantCode(lotId, participantId) {
  var hashData = lotId + participantId;
  var digest = hash(hashData);

  var group1 = digest.readUIntBE(0, 2) % 10000;
  var group2 = digest.readUIntBE(2, 2) % 10000;
  var group3 = digest.readUIntBE(4, 2) % 10000;
  // Luhn algorithm used to calculate check sum
  var checkSum = ((digitSum(group1) + digitSum(group2) + digitSum(group3)) * 9) % 10;
  var code = addLeadingZeroes(group1, 4) + "-" + addLeadingZeroes(group2, 4) + "-" + addLeadingZeroes(group3, 4) + "-" + checkSum;

  return code;
}

export function checkParticipantCode(code) {
  var regexp = "^(\\d+)-(\\d+)-(\\d+)-(\\d)$";
  var match = code.match(regexp);
  if (match) {
    var group1 = parseInt(match[1]);
    var group2 = parseInt(match[2]);
    var group3 = parseInt(match[3]);
    var checkSum = ((digitSum(group1) + digitSum(group2) + digitSum(group3)) * 9) % 10;

    return checkSum == parseInt(match[4]);

  } else {
    return false;
  }
}

function digitSum(number) {
  var tmp = number, sum = 0;
  while (tmp !== 0) {
    sum += tmp % 10;
    tmp = (tmp / 10) >> 0;
  }

  return sum;
}

function addLeadingZeroes(number, decimals) {
	var result = number.toString();
	var zeroes = decimals - result.length;
	for (var i = 0; i < zeroes; i++) {
    result = "0" + result;
  }

  return result;
}