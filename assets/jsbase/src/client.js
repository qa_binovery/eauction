import {xdr, hash} from "./index";
import {encodeCheck} from "./strkey";
import {Operation} from "./operation";
import {Network} from "./network";
import {map, each} from "lodash";
import {Keypair} from "./keypair";
import {Account} from "./account";
import {ExternalWalletStorage} from "./wallet";
import {TransactionBuilder} from "./transaction_builder";
import * as StellarBase from "../src";


//TODO: move api interaction to separate entity?
//TODO: refactor methods with 'callback; parameter

export class StellarClient {

    constructor(apiServer, keypair) {
        this.path = apiServer;
        this.createPath = apiServer + "/lots/create";
        this.seqPath = apiServer + "/application/next_seq";
        this.registerPath = apiServer + "/users/register";
        this.rootKeypair = keypair;
    }
    
    static registerForApproval(apiServer, accountId, codeKYC) {
        let data = {'destination': accountId, 'code_kyc': codeKYC, 'submit': 'register'};
        return $.post(apiServer + "/users/register", data);
    }

    static _registerForApprovalDebug(apiServer, keypair, codeKYC) {
        let seed = keypair.seed();
        let destination = keypair.accountId();
        let data = {'destination': destination, 'code_kyc': codeKYC, 'submit': 'register'};

        var deferred = $.Deferred();

        $.post(apiServer + "/users/register", data)
            .always(function (response) {
                deferred.resolve(new StellarClient(apiServer, keypair));
            });

        return deferred.promise();
    }
    
    createLot(lot, callback) {
        let xdrLot = new StellarBase.xdr.LotEntry();
        if (!lot.lotId)
            lot.lotId = StellarBase.Keypair.random().accountId();
        xdrLot.lotId = lot.lotId;
        xdrLot.lotName = lot.name;
        xdrLot.lotAddress = lot.address;
        xdrLot.lotLocation = lot.location;
        xdrLot.lotBranch = lot.branch;
        xdrLot.type = lot.type;
        xdrLot.lotCode = lot.code;
        xdrLot.startPrice = lot.startPrice;
        xdrLot.minStep = lot.minStep;
        xdrLot.maxStep = lot.maxStep;
        xdrLot.duration = lot.duration;
        xdrLot.published = lot.publicationTime; 
        xdrLot.aucStarted = lot.auctionStartTime; 
        xdrLot.pledge = lot.pledge;

        var details = {};
        details.pledgeConditions = lot.pledgeConditions;
        details.contact = lot.contact;
        //details.courtInfo = lot.courtInfo;
        //details.debts = lot.debts;
        //details.description = lot.description;
        details.specification = lot.specification;
        details.financialReport = lot.financialReport;
        details.additionalInfo = lot.additionalInfo;

        xdrLot.details = JSON.stringify(details);
        xdrLot.publisherId = lot.auctionPlatformId;
        this._submitLot(xdrLot, function(data) {
            data.lotId = lot.lotId;
            callback(data);
        });
    }

    changeLot(lot, callback) {
        let xdrLot = new StellarBase.xdr.LotEntry();
        xdrLot.lotId = lot.lotId;
        var details = {};
        details.pledgeConditions = lot.pledgeConditions;
        details.contact = lot.contact;
        //details.courtInfo = lot.courtInfo;
        //details.debts = lot.debts;
        //details.description = lot.description;
        details.specification = lot.specification;
        details.financialReport = lot.financialReport;
        details.additionalInfo = lot.additionalInfo;

        xdrLot.details = JSON.stringify(details);
        this._submitLotChange(xdrLot, function(data) {
            data.lotid = lot.lotId;
            callback(data);
        });
    }

    sendMessage(lotId, question, callback) {
        var message = new StellarBase.xdr.MessageEntry();
        message.lotId = lotId;
        message.text = question;
        message.messageId = StellarBase.Keypair.random().accountId();
        message.refMessage = message.messageId;
        this._submitMessage(message, callback);
    }

    replyToMessage(lotId, reply, parentMessageId, callback) {
        var message = new StellarBase.xdr.MessageEntry();
        message.lotId = lotId;
        message.messageId = StellarBase.Keypair.random().accountId();
        message.complaint = false;
        message.text = reply;
        message.refMessage = parentMessageId;
        this._submitMessage(message, callback);
    }

    complain(lotId, text, callback) {
        var message = new StellarBase.xdr.MessageEntry();
        message.lotId = lotId;
        message.messageId = StellarBase.Keypair.random().accountId();
        message.complaint = true;
        message.text = text;
        message.refMessage = parentMessageId;
        this._submitMessage(message, callback);
    }

    createBid(lotId, amount, callback) {
        this._submitBid(lotId, amount, callback);
    }

    affirmFail(lotId, account, purpose, callback) {
        this._affirmFail(lotId, account, purpose, callback);
    }

    affirmPass(lotId, account, paperHash, paperLink, callback) {
        this._affirmPass(lotId, account, paperHash, paperLink, callback);
    }

    participate(lotId, auctionPlatformId, callback) {
        this._submitParticipate(lotId, auctionPlatformId, callback);
    }
    
    _pushTx(transaction, callback) {
        let b64encoded = transaction.toEnvelope().toXDR('base64');
        $.post(this.createPath, { 'body':b64encoded, submit: "create" }).always(function(param) {
            callback(param);
        });
    }

    _nextSeq(callback) {
        var params = '?account=' + this.rootKeypair.accountId();
        $.get(this.seqPath, { 'account':this.rootKeypair.accountId() }).always(function(param) {
            callback(param);
        });
    }

    _getAccount(callback) {
        var self = this;
        this._nextSeq(function(result){
            let seq = parseInt(result);
            if (!seq)
                console.log(result);
            callback(new Account(self.rootKeypair.accountId(), seq-1));
        });
    }

    _getPlatformId(callback) {
        $.get(this.path + '/application/node_public_key').always(data => callback(data));
    }

    _submitLot(lot, callback) {
        var self = this;
        self._getAccount(function(account){
            let transaction = new TransactionBuilder(account)
                .addOperation(Operation.createLot({
                        lotId: lot.lotId,
                        lotName: lot.lotName,
                        lotAddress: lot.lotAddress,
                        lotLocation: lot.lotLocation,
                        type: lot.type,
                        lotBranch: lot.lotBranch,
                        lotCode: lot.lotCode,
                        aucStarted: lot.aucStarted,
                        startPrice: lot.startPrice,
                        minStep: lot.minStep,
                        maxStep: lot.maxStep,
                        duration: lot.duration,
                        published: lot.published,
                        pledge: lot.pledge,
                        details: lot.details,
                        publisherId: lot.publisherId
                    }))
                .build();
            transaction.sign(self.rootKeypair);
            self._pushTx(transaction, callback);
        });
    }

    _submitLotChange(lot, callback) {
        var self = this;
        self._getAccount(function(account){
            let transaction = new TransactionBuilder(account)
                .addOperation(Operation.changeLot({
                        lotId: lot.lotId,
                        details: lot.details,
                    }))
                .build();
            transaction.sign(self.rootKeypair);
            self._pushTx(transaction, callback);
        });
    }

    _submitMessage(message, callback) {
        var self = this;
        self._getAccount(function(account){
            let transaction = new TransactionBuilder(account)
                    .addOperation(Operation.sendMessage({
                        messageId: message.messageId,
                        lotId: message.lotId,
                        text: message.text,
                        refMessage: message.refMessage
                    }))
                .build();
            transaction.sign(self.rootKeypair);
            self._pushTx(transaction, function(data) {
                data.messageId = message.messageId;
                callback(data);
            });
        });
    }

    _submitBid(lotId, amount, callback) {
        var self = this;
        self._getAccount(function(account) {
            let transaction = new TransactionBuilder(account)
                .addOperation(Operation.createBid({
                    lotId: lotId,
                    amount: amount,
                    }))
                .build();
            transaction.sign(self.rootKeypair);
            self._pushTx(transaction, callback);
        });
    }

    _affirmFail(lot, acc, purp, callback) {
        var self = this;
        self._getAccount(function(account) {
            let transaction = new TransactionBuilder(account)
                .addOperation(Operation.affirmFail({
                    lotId: lot,
                    accountId: acc,
                    purpose: purp
                    }))
                .build();
            transaction.sign(self.rootKeypair);
            self._pushTx(transaction, function (response) {
                callback(response);
            });
        });
    }

    _affirmPass(lot, acc, hash, link, callback) {
        var self = this;
        self._getAccount(function(account) {
            let transaction = new TransactionBuilder(account)
                .addOperation(Operation.affirmPass({
                    lotId: lot,
                    accountId: acc,
                    paperHash: hash,
                    paperLink: link
                    }))
                .build();
            transaction.sign(self.rootKeypair);
            self._pushTx(transaction, callback);
        });
    }

    _submitParticipate(lotId, platformId, callback) {
        var self = this;
        self._getAccount(function(account){
            let transaction = new TransactionBuilder(account)
                .addOperation(Operation.registerParticipant({
                    lotId: lotId,
                    auctionPlatformId: platformId
                    }))
                .build();
            transaction.sign(self.rootKeypair);
            self._pushTx(transaction, callback);
        });
    }
}
