import StellarWallet from 'stellar-wallet-js-sdk';
import {Keypair} from "./keypair";
import {Account} from "./account";

//TODO: move scrypt params and wallet-server url to config file

export class EncryptedWalletStorage {

    /**
    * Store encrypted on the wallet-server.
    *
    * @param {string} server Wallet-server's address.
    * @param {string} username
    * @param {string} password
    * @param {Keypair} keypair
    * @param {string} domain User's domain.
    *
    * @returns {Wallet}
    */
    static registerWallet(server, keypair, username, password, domain) {
        return StellarWallet.createWallet({
            server: server  + '/v2',
            username: username + "@" + domain,
            password: password,
            publicKey: keypair.rawPublicKey().toString('base64'),
            keychainData: keypair.toJson(),
            mainData: 'mainData',
            kdfParams: {
                algorithm: 'scrypt',
                bits: 256,
                n: Math.pow(2,11),
                r: 8,
                p: 1
            }
        });
    }

    /**
   * Gets wallet from the wallet-server.
   *
   * @param {string} server Wallet-server's address.
   * @param {string} username
   * @param {string} password
   * @param {string} domain User's domain.
   *
   * @returns {Wallet}
   */
    static getWallet(server, username, password, domain) {
        let params = {
            server: server + '/v2',
            username: username.toLowerCase() + "@" + domain,
            password: password
        };

        return StellarWallet.getWallet(params)
            .then(wallet => {
                wallet.keypair = Keypair.fromJson(wallet.getKeychainData());
                return wallet;
            });
    }

}
