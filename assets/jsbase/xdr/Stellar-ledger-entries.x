// Copyright 2015 Stellar Development Foundation and contributors. Licensed
// under the Apache License, Version 2.0. See the COPYING file at the root
// of this distribution or at http://www.apache.org/licenses/LICENSE-2.0

%#include "xdr/Stellar-types.h"

namespace stellar
{

typedef PublicKey AccountID;
typedef opaque Thresholds[4];
typedef string string32<32>;
typedef uint64 SequenceNumber;

enum AssetType
{
    ASSET_TYPE_NATIVE = 0,
    ASSET_TYPE_CREDIT_ALPHANUM4 = 1,
    ASSET_TYPE_CREDIT_ALPHANUM12 = 2
};

union Asset switch (AssetType type)
{
case ASSET_TYPE_NATIVE: // Not credit
    void;

case ASSET_TYPE_CREDIT_ALPHANUM4:
    struct
    {
        opaque assetCode[4]; // 1 to 4 characters
        AccountID issuer;
    } alphaNum4;

case ASSET_TYPE_CREDIT_ALPHANUM12:
    struct
    {
        opaque assetCode[12]; // 5 to 12 characters
        AccountID issuer;
    } alphaNum12;

    // add other asset types here in the future
};

// price in fractional representation
struct Price
{
    int32 n; // numerator
    int32 d; // denominator
};

// the 'Thresholds' type is packed uint8_t values
// defined by these indexes
enum ThresholdIndexes
{
    THRESHOLD_MASTER_WEIGHT = 0,
    THRESHOLD_LOW = 1,
    THRESHOLD_MED = 2,
    THRESHOLD_HIGH = 3
};

enum LedgerEntryType
{
    ACCOUNT = 0,
    TRUSTLINE = 1,
    OFFER = 2,
    LOT = 3,
    PARTICIPANT = 4,
    MESSAGE = 5,
    PROOF = 6,
    AUCTION_PLATFORM = 7
};

struct Signer
{
    AccountID pubKey;
    uint32 weight; // really only need 1byte
};

enum AccountFlags
{ // masks for each flag

    // Flags set on issuer accounts
    // TrustLines are created with authorized set to "false" requiring
    // the issuer to set it for each TrustLine
    AUTH_REQUIRED_FLAG = 0x1,
    // If set, the authorized flag in TrustLines can be cleared
    // otherwise, authorization cannot be revoked
    AUTH_REVOCABLE_FLAG = 0x2,
    // Once set, causes all AUTH_* flags to be read-only
    AUTH_IMMUTABLE_FLAG = 0x4
};

/* AccountEntry

    Main entry representing a user in Stellar. All transactions are
    performed using an account.

    Other ledger entries created require an account.

*/

struct AccountEntry
{
    AccountID accountID;      // master public key for this account
    int64 balance;            // in stroops
    SequenceNumber seqNum;    // last sequence number used for this account
    uint32 numSubEntries;     // number of sub-entries this account has
                              // drives the reserve
    AccountID* inflationDest; // Account to vote for during inflation
    uint32 flags;             // see AccountFlags
    uint64 lastLotCreationTime;

    string32 homeDomain; // can be used for reverse federation and memo lookup

    // fields used for signatures
    // thresholds stores unsigned bytes: [weight of master|low|medium|high]
    Thresholds thresholds;

    Signer signers<20>; // possible signers for this account

    string32 codeKYC;
    // reserved for future use
    union switch (int v)
    {
    case 0:
        void;
    }
    ext;
};

/* ProofEntry

*/

struct ProofEntry
{
    AccountID accountID;
    PublicKey lotID;
    uint64 proofID;
    uint64 bankID;
    uint32 quantity;

    string proof<1000>;

    // reserved for future use
    union switch (int v)
    {
    case 0:
        void;
    }
    ext;
};

/* TrustLineEntry
    A trust line represents a specific trust relationship with
    a credit/issuer (limit, authorization)
    as well as the balance.
*/

enum TrustLineFlags
{
    // issuer has authorized account to perform transactions with its credit
    AUTHORIZED_FLAG = 1
};

struct TrustLineEntry
{
    AccountID accountID; // account this trustline belongs to
    Asset asset;         // type of asset (with issuer)
    int64 balance;       // how much of this asset the user has.
                         // Asset defines the unit for this;

    int64 limit;  // balance cannot be above this
    uint32 flags; // see TrustLineFlags

    // reserved for future use
    union switch (int v)
    {
    case 0:
        void;
    }
    ext;
};

enum OfferEntryFlags
{
    // issuer has authorized account to perform transactions with its credit
    PASSIVE_FLAG = 1
};

/* OfferEntry
    An offer is the building block of the offer book, they are automatically
    claimed by payments when the price set by the owner is met.

    For example an Offer is selling 10A where 1A is priced at 1.5B

*/
struct OfferEntry
{
    AccountID sellerID;
    uint64 offerID;
    Asset selling; // A
    Asset buying;  // B
    int64 amount;  // amount of A

    /* price for this offer:
        price of A in terms of B
        price=AmountB/AmountA=priceNumerator/priceDenominator
        price is after fees
    */
    Price price;
    uint32 flags; // see OfferEntryFlags

    // reserved for future use
    union switch (int v)
    {
    case 0:
        void;
    }
    ext;
};


/* LotEntry

*/

enum LotType {
    TERRITORY = 1,
    FACILITY = 2
};

enum LotBranch {
    AGRICULTURE = 1,
    LIVESTOCK = 2
};

struct LotEntry
{
    AccountID organizerID;
    PublicKey lotID;
    string lotName<1000>;
    uint32 type;
    string32 lotCode;
    string lotAddress<1000>;
    uint32 lotBranch;
    string lotLocation<1000>;
    uint64 startPrice;
    uint64 bestPrice;
    uint32 minStep;
    uint32 maxStep;
    uint64 published;
    uint64 aucStarted;
    uint64 duration;
    uint64 pledge;
    AccountID publisherID;
    string details<1000>;
    // reserved for future use
    union switch (int v)
    {
    case 0:
    void;
    }
    ext;
};

enum ParticipantState {
    APPROVED = 0, 
    PENDING = 1,
    PASS = 2,
    FAIL = 3
};
/* ParticipantEntry

*/
struct ParticipantEntry
{
    AccountID accountID;
    PublicKey lotID;
    uint32 state;
    uint64 bestBid;
    uint32 bestBidSeq;
    AccountID auctionPlatformID;
    string purpose<1000>;

    // reserved for future use
    union switch (int v)
    {
    case 0:
    void;
    }
    ext;
};


/* MessageEntry

*/
struct MessageEntry
{
    PublicKey messageID;
    AccountID accountID;
    string text<1000>;
    PublicKey lotID;
    PublicKey refMessageID;
    bool complaint;
    // reserved for future use
    union switch (int v)
    {
    case 0:
    void;
    }
    ext;
};

/* AuctionPlatformEntry

*/
struct AuctionPlatformEntry
{
    AccountID platformID;
    AccountID accountID;
    // reserved for future use
    union switch (int v)
    {
    case 0:
    void;
    }
    ext;
};

struct LedgerEntry
{
    uint32 lastModifiedLedgerSeq; // ledger the LedgerEntry was last changed

    union switch (LedgerEntryType type)
    {
    case ACCOUNT:
        AccountEntry account;
    case TRUSTLINE:
        TrustLineEntry trustLine;
    case OFFER:
        OfferEntry offer;
    case LOT:
        LotEntry lot;
    case PARTICIPANT:
        ParticipantEntry participant;
    case MESSAGE:
        MessageEntry message;
    case PROOF:
        ProofEntry proof;
    case AUCTION_PLATFORM:
        AuctionPlatformEntry auctionPlatformEntry;
    }
    data;

    // reserved for future use
    union switch (int v)
    {
    case 0:
        void;
    }
    ext;
};

// list of all envelope types used in the application
// those are prefixes used when building signatures for
// the respective envelopes
enum EnvelopeType
{
    ENVELOPE_TYPE_SCP = 1,
    ENVELOPE_TYPE_TX = 2,
    ENVELOPE_TYPE_AUTH = 3
};
}
