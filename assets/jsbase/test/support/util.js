import {Keypair} from '../../src/keypair';

export class TestUtils {

	static generateRandomString(length) {
		return Keypair.random().accountId().substr(0, length);
	}

	static timeNow() {
		return Math.trunc(new Date() / 1000);
	}
}