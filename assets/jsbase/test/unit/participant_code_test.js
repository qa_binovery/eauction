import {Keypair} from '../../src/keypair';
import {createParticipantCode, checkParticipantCode} from '../../src/participant_code';

describe('Participant code tests', function() {

	it ("Generate and check", function () {
		var lot = Keypair.random().accountId();
    	var participant = Keypair.random().accountId();
    	var code = createParticipantCode(lot, participant);

    	expect(checkParticipantCode(code)).to.be.true;
	});

	it ("Check code with invalid format", function () {
		expect(checkParticipantCode("dfasfasfas")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-77")).to.be.false;
	});

	it ("Check valid code", function () {
		var code = "2620-0506-7737-5";
		expect(checkParticipantCode(code)).to.be.true;
	});

	it ("Check code with invalid checkSum", function () {
		expect(checkParticipantCode("2620-0506-7737-0")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-1")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-2")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-3")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-4")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-6")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-7")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-8")).to.be.false;
		expect(checkParticipantCode("2620-0506-7737-9")).to.be.false;
	});

});
