import {EncryptedWalletStorage} from '../../src/wallet';
import {Keypair} from '../../src/keypair';

var server = 'http://eaucdocker.cloudapp.net:3000';

describe('Wallet server integration tests', function () {
  
	it('Get wallet', function (done) {

    var username = 'brand_new_user';
    var password = 'xxx';
    var domain = 'stellar.org';


    EncryptedWalletStorage.getWallet(server, username, password, domain)
      .then(wallet => {
        this.timeout(50000);
        var fromSeed = Keypair.fromBase58Seed("s3KBbii4LXfG2yjskg3wf4udxdLGCmsp3Y48S93tVbC2SEkkXyn");
        expect(wallet.keypair.seed()).to.eql(fromSeed.seed());
        done();
      })
      .catch(e => {
        console.log("Error: " + e.name);
        done(e);
      });
  });

it('Create wallet', function (done) {

    var keypair = Keypair.random();
    var username = keypair.accountId().substr(0, 7);
    var password = 'xxx';
    var domain = 'stellar.org';

    EncryptedWalletStorage.registerWallet(server, keypair, username, password, domain)
      .then(function(wallet) {
        expect(wallet.getMainData()).to.eql('mainData');
        done();
      });
  });

});