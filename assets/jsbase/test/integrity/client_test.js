/*
	To run integrity tests use: gulp integrity-test.
*/
import {Wallet} from '../../src/wallet';
import {Keypair} from '../../src/keypair';
import {StellarClient} from '../../src/client';
import {TestUtils} from '../support/util';
import * as StellarBase from "../../src";

var sleep = require('sleep');

var jsdom = require('node-jsdom');
var doc = jsdom.jsdom();
var window = doc.parentWindow;
global.$  = require('jquery')(window);

var apiServer = "http://127.0.0.1:3000";//"http://82.196.7.33:8001";
var walletServer = "http://82.196.7.33:3000";

var apiNodeId; //auction platform id, obtained from the api server


var client; //StellarCilent with lot owner's keypair
var clientKeypair;
var login = TestUtils.generateRandomString(7);
var password = 'xxx';
var domain = 'test.net';

var lotId;
var auctionDuration = 60;

var bidders;

describe("Client integrity tests", function () {
	
	it('Register lot owner', function (done) {
    this.timeout(50000);

    clientKeypair = StellarBase.Keypair.random();
    var kycCode = TestUtils.generateRandomString(32);
    console.log("KYC: " + kycCode);
		StellarClient._registerForApprovalDebug(apiServer, clientKeypair, kycCode)
        .done(function (newClient) {
          expect(newClient).to.not.null;
          client = newClient;
          done();
        });

  });

  it('Register bidders', function (done) {
    this.timeout(100000);

    var deferred = [];
    for (var i = 0; i < 5; i++) {
      var keypair = StellarBase.Keypair.random();
      var kyc = TestUtils.generateRandomString(32);
      console.log("KYC: " + kyc);
      var deferredBidder = StellarClient._registerForApprovalDebug(apiServer, keypair, kyc);
      deferred.push(deferredBidder);
    }

    $.when.apply(this, deferred).done(function (c1, c2, c3, c4, c5) {
      bidders = [];
      bidders.push(c1);
      bidders.push(c2);
      bidders.push(c3);
      bidders.push(c4);
      bidders.push(c5);

      done();
    });
  });

  it ('Get plarform id', function (done) {
    this.timeout(50000);

    client._getPlatformId(function (data) {
      console.log(data);
      apiNodeId = data['public_key'];
      done();
    });

  });
  it ('Create lot', function (done) {
    this.timeout(50000);
		var lot = {
      lotId: StellarBase.Keypair.random().accountId(),
			name: "Some goverment's property",
			address: "Sample address",
			location: "Sample location",
			branch: 0,
			type: 0,
			code: TestUtils.generateRandomString(32),
			startPrice: "1000",
			minStep: 2,
			maxStep: 40,
			duration: auctionDuration.toString(),
			publicationTime: (TestUtils.timeNow()).toString(),
			auctionStartTime: (TestUtils.timeNow() + 100).toString(),
			pledge: "100",
			details: "Sample details",
      contact: "My contact",
      pledgeConditions: "Some conditions",
			auctionPlatformId: apiNodeId.toString(),
      specification: { //технічна специфікація
          ref: "https://drive.google.com/laksjdfl/spec.pdf", //посилання на специфікацію на зовнішньому ресурсі
          hash: "A23423F23B" //контрольна сума файла
      },
      financialReport: { //фінансова звітність (для підприємств)
          ref: "https://drive.google.com/laksjdfl/report.pdf", //посилання на звітність на зовнішньому ресурсі
          hash: "A23423F245B" //контрольна сума файла
      },
      additionalInfo: { //додаткова інформація (документ)
          ref: "https://drive.google.com/laksjdfl/info.pdf", //посилання на інфо на зовнішньому ресурсі
          hash: "A23423F23B" //контрольна сума файла
      },
		}
    client.createLot(lot, function (param) {
      console.log(param);
      lotId = param.lotId;
      done();
    });
  });
  it ('Create lot second time from the same account', function (done) {
    this.timeout(50000);
    var lot = {
      lotId: StellarBase.Keypair.random().accountId(),
      name: "Some goverment's property",
      address: "Sample address",
      location: "Sample location",
      branch: 0,
      type: 0,
      code: TestUtils.generateRandomString(32),
      startPrice: "1000",
      minStep: 2,
      maxStep: 40,
      duration: auctionDuration.toString(),
      publicationTime: (TestUtils.timeNow()).toString(),
      auctionStartTime: (TestUtils.timeNow() + 100).toString(),
      pledge: "100",
      details: "Sample details",
      contact: "My contact",
      pledgeConditions: "Some conditions",
      auctionPlatformId: apiNodeId.toString(),
      specification: { //технічна специфікація
          ref: "https://drive.google.com/laksjdfl/spec.pdf", //посилання на специфікацію на зовнішньому ресурсі
          hash: "A23423F23B" //контрольна сума файла
      },
      financialReport: { //фінансова звітність (для підприємств)
          ref: "https://drive.google.com/laksjdfl/report.pdf", //посилання на звітність на зовнішньому ресурсі
          hash: "A23423F245B" //контрольна сума файла
      },
      additionalInfo: { //додаткова інформація (документ)
          ref: "https://drive.google.com/laksjdfl/info.pdf", //посилання на інфо на зовнішньому ресурсі
          hash: "A23423F23B" //контрольна сума файла
      },
    }
    client.createLot(lot, function (param) {
      console.log(param);
      done();
    });
  })
	it ('Get lot', function (done) {
    this.timeout(50000);

		$.get(apiServer + '/lots', { 'publisher':apiNodeId})
        .done(function(data) {
          var lastLot = data[0];
          console.log(lastLot.lotId);
          done();
        })
        .fail(function (e) {
          done(e);
        });
	});


  it ('Change lot', function (done) {
    this.timeout(50000);
    console.log("Lot id: " + lotId);
    var lot = {
      lotId: lotId,
      contact: "dasdasdasdasd",
      pledgeConditions: "sadasdasdads",
      specification: { //технічна специфікація
          ref: "https://drive.google.com/laksjdfl/spec.pdf", //посилання на специфікацію на зовнішньому ресурсі
          hash: "A23423F23B" //контрольна сума файла
      },
      financialReport: { //фінансова звітність (для підприємств)
          ref: "https://drive.google.com/laksjdfl/report.pdf", //посилання на звітність на зовнішньому ресурсі
          hash: "A23423F245B" //контрольна сума файла
      },
      additionalInfo: { //додаткова інформація (документ)
          ref: "https://drive.google.com/laksjdfl/info.pdf", //посилання на інфо на зовнішньому ресурсі
          hash: "A23423F23B" //контрольна сума файла
      },
    }

    client.changeLot(lot, function (param) {
      console.log(param);
      done();
    });
    console.log("Lot changed by: " + client.rootKeypair.accountId());
  })

	it ('Register participant', function (done) {
    this.timeout(5000000);

    var submitted = 0;

    bidders.forEach(function (bidder) {
      bidder.participate(lotId.toString(), apiNodeId.toString(), function() {
        $.post(apiServer + '/lots/approve_participant', 
          {
            account_id : bidder.rootKeypair.accountId(),
            lot_id : lotId.toString(),
            bank_public_key: "GDORKBCHJIH3RNYDXV3BV5Y4E3IMQKPBCBTG7Y3ZKO4HW5BFA7K7M3X7",
            sign_data: "test",
            signature: "test",
            deposit: 0
          }
          )
          .always(function (response) {
            $.post(apiServer + '/lots/approve_participant', 
            {
              account_id : bidder.rootKeypair.accountId(),
              lot_id : lotId.toString(),
              bank_public_key: "GDORKBCHJIH3RNYDXV3BV5Y4E3IMQKPBCBTG7Y3ZKO4HW5BFA7K7M3X7",
              sign_data: "test",
              signature: "test",
              deposit: 1
            });
            submitted++;
            if (submitted == bidders.length) {
              done();
            }
          });
      });
    });
	});
it ('Submit bids', function (done) {
  this.timeout(5000000);
  //TODO: check tx status in callbacks
  var submittedBids = 0;

  bidders.forEach(function (bidder) {
    bidder.createBid(lotId.toString(), "1010",  function(response) {
      submittedBids++;
      if (submittedBids == bidders.length) {
        done();
      }
    });
  });
});
var messageId;
it ('Send message', function (done) {
  this.timeout(50000);
  //TODO: check tx status in callback
   bidders[0].sendMessage(lotId, 'Sample text', function (response) {
    console.log(response);
    messageId = response.messageId;
    done();
  });
});

it ('Reply message', function (done) {
  this.timeout(50000);
  //TODO: check tx status in callback
   client.replyToMessage(lotId, 'Sample text', messageId, function (response) {
    console.log(response);
    done();
  });
});


it ('Affirm fail', function (done) {
  this.timeout(50000);
  //sleep.sleep(auctionDuration);

  var topId = bidders[0].rootKeypair.accountId();
  console.log("Failing him: " + topId);
  //TODO: check tx status in callback
  client.affirmFail(lotId.toString(), topId, "purpose", function (response) {
    console.log(response);
    done();
  });
  console.log("Affirm fail by: " + client.rootKeypair.accountId());
});

it ('Affirm pass', function (done) {
  this.timeout(50000);

  var newTopId = bidders[1].rootKeypair.accountId();
  var paperHash = TestUtils.generateRandomString(500);
  var paperLink = TestUtils.generateRandomString(500);
  console.log("Passing him: " + newTopId);
  client.affirmPass(lotId.toString(), newTopId, paperHash, paperLink, function (response) {
    console.log(response);
    done();
  });
  //TODO: check tx status in callback
});

});