# JS Auction Base

## Registering a new user:
Upon registration the user provides KYC fields and the platform uses it to create an account in the ledger.

- on the client (js lib):
  * user generates a key pair (seed and accountId)

 ```js
    var keypair = StellarBase.Keypair.random();
 ```
  * he sends his AccountID together with KYC information to the platform for approve.

 ```js
    var apiServer = "superplatform.com:8001";
    var accountId = keypair.accountId();
	  var codeKYC = "ІНН: 1345934534343";
    
    StellarClient.registerForApproval(apiServer, accountId, codeKYC)
      .then(function (response) {
	       //process response
      });
    
 ```

 > NOTE: In this sample the registration is performed upon user's call to `apiServer + "/users/register"` which means the KYC validation is not performed. In the production environment the registration should be performed after KYC validation from the server's backend.

  * and registers his encrypted seed on remote wallet-server:

 ```js
    var walletServer = "superplatform.com:3000"
    var username = "<username_is_here>";
    var password = 'xxx';
    var domain = 'super_platform.org';

    EncryptedWalletStorage.registerWallet(walletServer, keypair, login, password, domain)
      .then(function (walletServerResponse) {
          // process wallet-server response
      });
 ```
- on the server

 The implementation of `apiServer + "/users/register"` registers users automatically. It should be isolated from direct access by the end user and called by platform backend when it verifies the KYC.

 ?????create_user

## Client

Client can retrieve his private key from the wallet server using:
```javascript
    var username = '<username_is_here>';
    var password = 'xxx';
    var domain = 'super_platform.org';
	  
    EncryptedWalletStorage.getWallet(walletServer, login, password, domain)
      .then(function (wallet) {
          // property wallet.keypair contains decrypted keypair
      });


```

Client uses his key to create StellarClient instance, which facilitates methods for lot creation, lot participation, e.t.c:

```javascript
        var client = new StellarClient(apiServer, keypair);
```

* create a lot

```javascript
var platformId = "GD5ZMNGHMF7SFFUHTMJOFEZI6OBK7AE6G63PD3YKWPICAN7L6ARIYJ6W"; //айді майданчика можна отримати на апі сервері
var lot = {
    lotId: "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB", // опцiональний айдi лоту, може бути згенерованим як StellarBase.Keypair.random().accountId()
    name: "Земельна ділянка в Одеській області", //назва лота аукціона
    address: "Одеська область", //адреса
    location: "{46.2818, 30.4237}", //геолокація
    type: LotType.Territory, //тип об'єкта
    branch: LotBranch.Agriculture, //галузь діяльності, 
    code: "<кадастровий код>",
    startPrice: "1000", //початкова ціна
    minStep: "2", //мінімальний крок аукціона, %
    maxStep: "30", //максимальний крок аукціона, %
    duration: 120, //тривалість аукціона, хвилини
    publicationTime: <publication_time>, //час публікації (час створення об'єкта)
    auctionStartTime: <start_time>, //час початку прийому ставок
    pledge: "10", //депозитний внесок (забезпечення)
	pledgeConditions: "Умови поверненя/неповернення забезпечення", //обов'язкове поле, коли забезпечення більше 0
	contact: "прізвище, ім'я та по батькові, посада та адреса однієї чи кількох посадових осіб організатора аукціону, уповноважених здійснювати зв'язок з учасниками",
	specification: { //технічна специфікація
		ref: "https://drive.google.com/laksjdfl/spec.pdf", //посилання на специфікацію на зовнішньому ресурсі
		hash: "A23423F23B" //контрольна сума файла
	},
	financialReport: { //фінансова звітність (для підприємств)
		ref: "https://drive.google.com/laksjdfl/report.pdf", //посилання на звітність на зовнішньому ресурсі
		hash: "A23423F245B" //контрольна сума файла
	},
	additionalInfo: { //додаткова інформація (документ)
		ref: "https://drive.google.com/laksjdfl/info.pdf", //посилання на інфо на зовнішньому ресурсі
		hash: "A23423F23B" //контрольна сума файла
	},
    auctionPlatformId: platformId //ідентифікатор майданчика 
}

client.createLot(lot, function (response) {
    // process API response
});
```
Response:

```javascript
{
    'state': 0, //стан: 0 - успіх, більше 0 - помилка
	'lotId': "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB",
	'info': nil // містить помилку при state > 0, рядок
}
```


* register to participate for a lot

```javascript
var lotId = "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB"; //айді лота, можна отримати з методу lots api сервера
client.participate(lotId, function(response) {
    done();//callback
  }));)
```
Response:

```javascript
{
    'state': 0, //стан: 0 - успіх, більше 0 - помилка
	'info': nil // містить помилку при state > 0, рядок
}
```

* send a question/complain/answer

Ask:

```javascript
var lotId = "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB"; //айді лота, можна отримати з методу lots api сервера
var question = "Питання?"; //текст питання
client.sendMessage(lotId, question, function(response) {
    done();//callback
  }));)
```

Complain is the same as question but use method complain: `client.complain(lotId, question, function(response) {});`
Complains can be sent only by participants


```javascript
var lotId = "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB"; //айді лота, можна отримати з методу lots api сервера
var reply = "Відповідь"; //текст відповіді
var parentMessageId = "GANVYFRWZSUMOCWBCJSKR67GSBI7THZ32TXMUFLC35PPEI2QNUUPCHZA"; //id питання
client.replyToMessage(lotId, reply, parentMessageId, function(response) {
    done();//callback
  }));)
```


Response:

```javascript
{
    'state': 0, //стан: 0 - успіх, більше 0 - помилка
	'messageId': "GANVYFRWZSUMOCWBCJSKR67GSBI7THZ32TXMUFLC35PPEI2QNUUPCHZA", //згенероване id повідомлення
	'info': nil // містить помилку при state > 0, рядок
}
```


* Bid for a lot

```javascript
var lotId = "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB"; //айді лота, можна отримати з методу lots api сервера
var amount = "1012"; //розмір ставки
client.createBid(lotId, amount, function(response) {
    done();//callback
  };)
```

Response:

```javascript
{
    'state': 0, //стан: 0 - успіх, більше 0 - помилка
	'info': nil // містить помилку при state > 0, рядок
}
```

* Winner fails

```javascript
var lotId = "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB"; //айді лота, можна отримати з методу lots api сервера
var purpose = "причина"; //причина відмови
var participantId = "GAWAANSILQ7BXXQJXRNBNVOJV3EQ7BBF6KOXFGOFX2PWLGFU4OUKQIF3"; // ідентифікатор учасника аукціона, котрий не пройшов compliance
client.affirmFail(lotId, participantId, purpose,  function(response) {
    done();//callback
  });
```


* Declare winner

```javascript
var lotId = "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB"; //айді лота, можна отримати з методу lots api сервера
var participantId = "GAN3KIQYIULB6UUUPSUOXLN65DLF3UKDNCL72PT2TOEYIVFJR7DGFR26"; // ідентифікатор переможця
var paperLink = "https://drive.google.com/laksjdfl/contract.pdf";//посилання на контракт на зовнішньому ресурсі
var paperHash = "sha256 base64 hash"; //контрольна сума файла
client.affirmPass(lotId, participantId, paperHash, paperLink,  function(response) {
    done();//callback
  });
```

##Api server

Get node's public key 

`GET-> <api_server>/application/node_public_key`

Response:

```javascript
{
    'public_key': "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB"; //публічний ключ майданчика/ноди
}
```

Get lots: 
```
GET-> <api_server>/lots
params:
	active - (bool), //фільтрувати за активними лотами
	organizer - accountId, //фільтрувати за організатором аукціона
	publisher - publisherId, //фільтрувати за майданчиком організатора
	participant - accountId, //повертати тільки аукціони, в котрих приймає участь окремий користувач
	type - (int), // фільтрація за типом об'єкта аукціона
	branch - (int) // фільтрація за галуззю діяльності
```


```javascript
[{
	lotId: "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB", //унікальний ідентифікатор лота
	organizerCode: "12345678", //ЕДРПО код організатра аукціона
	platformCode:  "12345678", //ЕДРПО код майданчика організатора аукціона
    name: "Земельна ділянка в Одеській області", //назва лота аукціона
    address: "Одеська область", //адреса
    location: "{46.2818, 30.4237}", //геолокація
    type: LotType.Territory, //тип об'єкта
    branch: LotBranch.Agriculture, //галузь діяльності, 
    code: "<кадастровий код>",
    startPrice: "1000", //початкова ціна
    minStep: "2", //мінімальний крок аукціона, %
    maxStep: "30", //максимальний крок аукціона, %
    duration: 120, //тривалість аукціона, хвилини
    publicationTime: <publication_time>, //час публікації (час створення об'єкта)
    auctionStartTime: <start_time>, //час початку прийому ставок
    pledge: "10", //депозитний внесок (забезпечення)
	pledgeConditions: "Умови поверненя/неповернення забезпечення", //обов'язкове поле, коли забезпечення більше 0
	contact: "прізвище, ім'я та по батькові, посада та адреса однієї чи кількох посадових осіб організатора аукціону, уповноважених здійснювати зв'язок з учасниками",
	specification: { //технічна специфікація
		ref: "https://drive.google.com/laksjdfl/spec.pdf", //посилання на специфікацію на зовнішньому ресурсі
		hash: "A23423F23B" //контрольна сума файла
	},
	financialReport: { //фінансова звітність (для підприємств)
		ref: "https://drive.google.com/laksjdfl/report.pdf", //посилання на звітність на зовнішньому ресурсі
		hash: "A23423F245B" //контрольна сума файла
	},
	additionalInfo: { //додаткова інформація (документ)
		ref: "https://drive.google.com/laksjdfl/info.pdf", //посилання на інфо на зовнішньому ресурсі
		hash: "A23423F23B" //контрольна сума файла
	},
    auctionPlatformId: platformId //ідентифікатор майданчика організатора
 }
]
```

```
GET <api_server>/participants/<lotId>
```

Де `lotId` - ідентифікатор лота

```javascript
[{
	lotId: "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB", //унікальний ідентифікатор лота
	participantId: "GAN3KIQYIULB6UUUPSUOXLN65DLF3UKDNCL72PT2TOEYIVFJR7DGFR26"; // ідентифікатор учасника
	participantCode: "923482309", // код ІНН/ЕДРПО учасника
	state: 0, //статус
	bestBid: "1013" //краща ставка учасника
 },
 {
	lotId: "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB",
	participantId: "GAWAANSILQ7BXXQJXRNBNVOJV3EQ7BBF6KOXFGOFX2PWLGFU4OUKQIF3";
	participantCode: "234234",
	state: 0, 
	bestBid: "1025"
 }
]
```

Get messages:
```
GET-> <api_server>/messages/messageId //повертає повідомлення за його id

GET-> <api_server>/messages //отримання повідомленнь за фільтром
params:
	answered - (bool), //фільтрувати за наявністю відповіді
	lot - lotId, // фільтрувати за лотом
	accountId - accountId, //повертати повідомлення окремого користувача
	organizer - accountId, //фільтрувати питання за організатором аукціона
```

```javascript
[
{
  messageId: "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB", //унікальний ідентифікатор повідомлення
  accountId: "GAN3KIQYIULB6UUUPSUOXLN65DLF3UKDNCL72PT2TOEYIVFJR7DGFR26"; // ідентифікатор учасника
  text: "Some message", // текст повідомлення
  lotId: "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB", //унікальний ідентифікатор лота
  refMessageId: "GDHZNRCLJ7BSYJSJX3DJFK2SXUUVULFVFQVCF2WQPSP7Y6KDF5AVFIZB", //унікальний ідентифікатор повідомлення на яке вiдповiдали для вiдповiдей, iнакше дорiвнює messageId
  isComplaint: "1", // чи є повiдомлення скаргою
  authorCode: "4124214", //ЕДРПО код автора
  organizertId: айдi організатора аукціона
 }
]
```



Upon registration as a participant for the lot client gets his code for a bank payment:

Platform approves the user by retrieving the appropriate tx from the bank and pushing it to the ledger. To get the transaction from the bank the platform gets the code by executing

getPaymentCode(account_id, lot_id)

For PrivatBank the platform will also need to get document number (номер квитанції)

Authentication

```javascript
{
    state: 0,
	info: "Все пройшло"
}
```


## Quick start

Using npm to include js-stellar-base in your own project:
```shell
npm install --save stellar-base
```

For browsers, [use Bower to install it](#to-use-in-the-browser). It exports a
variable `StellarBase`. The example below assumes you have `stellar-base.js`
relative to your html file.

```shell
sudo npm install
sudo npm install gulp
gulp
```

```html
<script src="stellar-base.js"></script>
<script>console.log(StellarBase);</script>
```

## Install

### To use as a module in a Node.js project
1. Install it using npm:

  ```shell
  npm install --save stellar-base
  ```
2. require/import it in your JavaScript:

  ```js
  var StellarBase = require('stellar-base');
  ```

### To use in the browser
1. Install it using [bower](http://bower.io):

  ```shell
  bower install stellar-base
  ```

2. Include it in the browser:

  ```html
  <script src="./bower_components/stellar-base/stellar-base.js"></script>
  <script>console.log(StellarBase);</script>
  ```

Note that you can also copy built JS files from [bower-js-stellar-base repo](https://github.com/stellar/bower-js-stellar-base) if you don't want to use Bower.

### To develop and test js-stellar-base itself
1. Clone the repo

  ```shell
  git clone https://github.com/stellar/js-stellar-base.git
  ```
2. Install dependencies inside js-stellar-base folder

  ```shell
  cd js-stellar-base
  npm install
  ```

## Usage
For information on how to use js-stellar-base, take a look at the docs in the [docs folder](./docs).

## Testing
To run all tests:
```shell
gulp test
```

To run a specific set of tests:
```shell
gulp test:node
gulp test:browser
```

Tests are also run on the [Travis CI js-stellar-base project](https://travis-ci.org/stellar/js-stellar-base) automatically.

## Documentation
Documentation for this repo lives inside the [docs folder](./docs).

## Contributing
Please see the [CONTRIBUTING.md](./CONTRIBUTING.md) for details on how to contribute to this project.

## Publishing to npm
```
npm version [<newversion> | major | minor | patch | premajor | preminor | prepatch | prerelease]
```
A new version will be published to npm **and** Bower by Travis CI.

npm >=2.13.0 required.
Read more about [npm version](https://docs.npmjs.com/cli/version).

## License
js-stellar-base is licensed under an Apache-2.0 license. See the [LICENSE](./LICENSE) file for details.

