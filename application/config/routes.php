<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = "home";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



$route['lot/view/([a-zA-Z0-9-]+)'] = "lot/index/$1";

$route['lot/create'] = "lot/create";