<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	}
	
	public function index()	{
		if(!$this->session->userdata('logged')){
			redirect(base_url().'login');
		}
		//echo $this->session->userdata('logged');
		$data = array();
		
		//$data['lots'] = $this->db->get_where('lots')->result_array();
		//$lots = file_get_contents('http://5.9.6.118:8001/lots');
		
		//$data['lots'] = json_decode($lots, true);
		//echo json_encode($lots);
		//print_r($data['lots']);
		$this->load->view('layouts/header', $data);
		$this->load->view('home', $data);
		$this->load->view('layouts/footer', $data);
	}
}
