<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lot extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	}
	
	public function index($lot)	{
		if(!$this->session->userdata('logged')){
			redirect(base_url().'login');
		}
		
		$data = array();
		
		$data['lot'] = $lot; //$this->db->get_where('lots', array('lotid'=>$lot))->row_array();
		// $data['lot']['details'] = json_decode($data['lot']['details']);
		
		
		
		
		//print_r($data['lots']);
		$this->load->view('layouts/header', $data);
		$this->load->view('lot', $data);
		$this->load->view('layouts/footer', $data);
	}
	
	public function create() {
		if(!$this->session->userdata('logged')){
			redirect(base_url().'login');
		}
		$data = array();
		$this->load->view('layouts/header', $data);
		$this->load->view('create', $data);
		$this->load->view('layouts/footer', $data);
	}
	
	public function addlot($platform) {
		$data = array();
		$data['lot'] = $_POST;
		$data['platform'] = $platform;
		echo json_encode($data['lot']);
	}
	
}
