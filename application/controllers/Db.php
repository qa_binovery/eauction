<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Db extends CI_Controller {

	public function index()	{
		$data = array();
		$query = $this->db->query("SELECT * FROM information_schema.tables WHERE table_schema='public'");
		$res = $query->result_array();
		foreach ($res as $k => $r) {
			$data = $this->db->query("SELECT * FROM ".$r['table_name']);
			$res[$k]['data'] = $data->result_array();
			
		}
		$this->load->view('db', array('tables'=>$res));
	}
	
	public function table($table='') {
		if ($table) {
			$data = array();
			$query = $this->db->query("SELECT * FROM ".$table);
			$res = $query->result_array();
			$this->load->view('table', array('table'=>$res));
		} else {
			redirect(base_url().'db');
		}
	}
}
