<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
	}
	
	public function index()	{
		if($this->session->userdata('logged')){
			redirect(base_url());
		}
		$data = array();
		
		$this->load->view('layouts/header', $data);
		$this->load->view('login', $data);
		$this->load->view('layouts/footer', $data);
	}
	
	public function auth() {
		$data = array();
		//$data['accountid']=$_POST['accountId'];
		$data=$_POST;
		
		$data['logged']=TRUE;
		$this->session->set_userdata($data);
		
		// $result = $this->db->get_where('accounts', $data);
		// if ($result->num_rows()) {
			// //$newdata = array('logged' => TRUE, 'accountid'=>$data['accountid']);
			// $data['logged']=TRUE;
			// $this->session->set_userdata($data);
		// }
		//redirect(base_url());
		
		echo json_encode($data);
	}
	
	public function register() {
		if($this->session->userdata('logged')){
			redirect(base_url());
		}
		$data = array();
		
		if (isset($_POST)) {
			$this->reg($_POST);
			$data = $_POST;
		}
		
		$this->load->view('layouts/header', $data);
		$this->load->view('login', $data);
		$this->load->view('layouts/footer', $data);
	}
	public function reg($data) {
		$data=$_POST;
		$data['balance'] =0;
		$data['seqnum'] =0;
		$data['numsubentries'] =0;
		$data['homedomain'] = '';
		$data['thresholds'] ='AQAAAA==';
		$data['lastmodified'] = 0;
		$data['flags'] = 0;
		
		$res = $this->db->insert('accounts', $data);
	}
	
	function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}
	
	
	
}