<!DOCTYPE html>
<html>
<head>
	<title>PostgreSQL</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("ul li a").on('click', function(){
				var url = $(this).attr('href');
				$("#content").load(url+" #content > *");
				return false;
			});
		});
	</script>
	<style>
		#sidebar {
			width: 20%;
			float:left;
			
		}
		#content {
			width: 80%;
			float:left;
			
		}
		table {
			width:100%;
			
		}
		table thead {
			background:#f9f9f9;
			
		}
		table td {
			padding:5px;
			
		} 
		
	</style>
</head>
<body>
	<div id="page">
		<header>
			PostgreSQL
		</header>
		<section id="sidebar">
			<ul>
				<?php foreach ($tables as $key => $tab) { ?>
					<li><a href="/db/table/<?=$tab['table_name']?>"><?=$tab['table_name']?></a></li>
				<?php } ?>
			</ul>
		</section>
		
		<section id="content">
			
		</section>
	</div>
</body>

</html>