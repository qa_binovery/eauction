<div id="container" class="clearfix">
    <div id="nav-bar">
        <a href="#" class="create-new-lot">Створити новий лот <i>+</i></a>
        <ul>
            <li><a href="#">Мої активні лоти</a></li>
            <li><a href="#">Мої готові лоти</a></li>
            <li><a href="#">Глобальний список лотів</a></li>
        </ul>
    </div>
    <main id="create-lot" class="main">
    	
    	<!-- <pre><?php print_r($_SESSION) ?></pre> -->
    	
        <div class="title-line">Новий лот</div>
        <form action="#" class="create-form" id="lotform" method="post">
        	<input type="hidden" name="accountId" id="accountId" value="<?=$_SESSION['accountId']?>" />
            <div class="column upload-doc">
                <div class="input-box">
                    <input type="file">
                    <p>Завантажити документ</p>
                </div>
            </div>
            <div class="column set-fields">
                <h3>Характеристика лоту</h3>
                <label>
                    <span>Найменування організатора земельних торгів</span>
                    <input type="text" required name="author" id="author">
                </label>
                <label>
                    <span>Назва лоту</span>
                    <input type="text" required name="title" id="lotname">
                </label>
                <label>
                    <span>Кадастровий номер земельної ділянки</span>
                    <input type="text" required name="knumber">
                </label>
                <label>
                    <span>Площа</span>
                    <input type="text" required name="yardage">
                </label>
                <label>
                    <span>Цільове призначення земельної ділянки (код КВЦПЗ)</span>
                    <textarea name="" id=""></textarea>
                </label>
                <label>
                    <span>Місце розташування земельної ділянки</span>
                    <textarea name="address" id="address"></textarea>
                </label>
                <label>
                    <span>Види використання земельної ділянки (для містобудівних потреб)</span>
                    <textarea name=""></textarea>
                </label>
                <label>
                    <span>Містобудівні умови і обмеження забудови земельної ділянки (для земельних ділянок, призначених для цілей, пов’язаних із забудовою)</span>
                    <textarea name=""></textarea>
                </label>

                <!-- <label class="rent">
                    <span>Назва лоту</span>
                    <input type="text" required name="title">
                </label>
                <label class="rent">
                    <span>Кадастровий номер земельної ділянки</span>
                    <input type="text" required name="knumber">
                </label>
                <label class="rent">
                    <span>Площа</span>
                    <input type="text" required name="yardage">
                </label>
                <label class="rent">
                    <span>Цільове призначення земельної ділянки (код КВЦПЗ)</span>
                    <textarea name=""></textarea>
                </label>
                <label class="rent">
                    <span>Місце розташування земельної ділянки</span>
                    <textarea name=""></textarea>
                </label>
                <label class="rent">
                    <span>Види використання земельної ділянки (для містобудівних потреб)</span>
                    <textarea name=""></textarea>
                </label>
                <label class="rent">
                    <span>Містобудівні умови і обмеження забудови земельної ділянки (для земельних ділянок, призначених для цілей, пов’язаних із забудовою)</span>
                    <textarea name=""></textarea>
                </label> -->
            </div>
            <div class="column set-fields contract-terms">
                <h3>Умови договору</h3>
                <div class="select-holder">
                    <span>Тип терміну дії договору</span>
                    <select name="type" id="lottype" class="selectpicker contract-terms-select">
                        <option value="0">Купівля-продаж</option>
                        <option value="1">Оренда</option>
                        <option value="2">Суперфіцій</option> 
                        <option value="3">Емфітевзис</option>
                    </select>
                </div>
                <label>
                    <span>Експертна грошова оцінка земельної ділянки</span>
                    <input type="text" required name="money">
                </label>
                <label>
                    <span>Стартова ціна продажуземельної ділянки</span>
                    <input type="text" required name="stprice" id="stprice">
                </label>
                <label>
                    <span>Крок торгів</span>
                    <input type="text" required name="steps" id="steps">
                </label>
                <label>
                    <span>Умови продажу земельної ділянки (розстрочення платежу, одноразовий платіж)</span>
                    <input type="text" required name="conditions" id="conditions">
                </label>
                <!-- <label class="rent">
                    <span>Строк користування земельною ділянкою та інші умови користування</span>
                    <input type="text" required name="time">
                </label>
                <label class="rent">
                    <span>Нормативна грошова оцінка земельної ділянки1</span>
                    <input type="text" required name="">
                </label>
                <label class="rent">
                    <span>Стартовий розмір річної плати за користування земельною ділянкою1</span>
                    <input type="text" required name="stprice">
                </label>
                <label class="rent">
                    <span>Крок торгів1</span>
                    <input type="text" required name="step">
                </label>
                <label class="rent">
                    <span>Сума коштів за підготовку лоту, які переможець повинен сплатити виконавцю земельних торгів1</span>
                    <input type="text" required>
                </label> -->
                <input type="submit" value="Створити лот" class="create-btn" id="createlot">
            </div>
        </form>
    </main>
</div>