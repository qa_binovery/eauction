<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	
	<style>
		#sidebar {
			width: 20%;
			float:left;
			
		}
		#content {
			width: 80%;
			float:left;
			overflow: auto;
		}
		table {
			width:100%;
			
		}
		table thead {
			background:#f9f9f9;
			
		}
		table td {
			padding:5px;
			
		} 
		
	</style>
</head>
<body>
	<div id="page">
		<header>
			PostgreSQL
		</header>
		<section id="sidebar">
			<ul>
				<?php foreach ($table as $key => $tab) { ?>
					<li><a href="/db/table/<?=$tab['table_name']?>"><?=$tab['table_name']?></a></li>
				<?php } ?>
			</ul>
		</section>
		
		<section id="content">
			<table>
				<thead>
					<tr>
					<?php foreach ($table[0] as $k => $t) { ?>
						<td><?=$k?></td>
					<?php } ?>
					</tr>
				</thead>
			
			<?php foreach ($table as $key => $tab) { ?>
				<tr>
					
					<?php foreach ($tab as $k => $t) { ?>
						<td><?=$t?></td>
						
					<?php } ?>
					
				</tr>
			<?php } ?>
			</table>
		</section>
	</div>
</body>

</html>