<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sign in/Sign up</title>
        <link media="all" rel="stylesheet" href="/assets/css/bootstrap.css">
        <link media="all" rel="stylesheet" href="/assets/css/bootstrap-select.css">
        <link media="all" rel="stylesheet" href="/assets/css/style.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto:300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        
        <link media="all" rel="stylesheet" href="/assets/css/common.css">
        
        <script src="/assets/jsbase/dist/stellar-base.js"></script>
        <script src="/assets/jsbase/dist/stellar-base.min.js"></script>
        
        <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWuowywyT5sfn5mAVCK5yuBljq8n53H5M&callback=initMap" async defer></script> -->
        
    </head>