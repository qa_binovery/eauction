<?php $this->load->view('layouts/main'); ?>
    <body>
        <div id="wrapper">
            <header id="header">
                <a href="/" id="logo">
                    <span>eAuction</span>
                </a>
                <?php if($this->session->userdata('logged')){ ?>
                <span id="logout" class="sign-in-title"><a href="/login/logout">Вихід</a></span>
                <?php } else { ?>
                <span id="auth" class="sign-in-title">Вхід</span>
                <?php } ?>
            </header>