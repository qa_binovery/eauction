<div id="container">
    <main id="main-sign">
        <div class="sign-in-block-holder">
            <div class="sign-in-block">
                <form action="/login/auth" method="post">
                    <input type="text" name="username" id="lusername" class="form-control" placeholder="Ім'я користувача">
                    <input type="text" name="password" id="lpassword" class="form-control" placeholder="Пароль">
                    <input type="text" name="domain" id="ldomain" class="form-control" placeholder="Домен">
                    <input type="submit" class="btn" value="Увійти" id="login">
                </form>
                <span>Забули пароль?</span>
                <a href="#" class="click-here">Натисніть сюди</a>
            </div>
        </div>
        <div class="sign-up-block-holder">
            <div class="sign-up-block">
                <form action="/login/register" method="post" class="register">
                    <input type="text" class="form-control" placeholder="Ім'я користувача" id="username"> <!-- name="username" -->
                    <input type="text" class="form-control" placeholder="Пароль" id="password"> <!-- name="password" -->
                    <input type="text" class="form-control" placeholder="Домен" id="domain"> <!-- name="repassword" -->
                    <input type="hidden" name="accountid" id="AccountID" class="form-control">
                    <input type="hidden" name="codekyc" id="Seed" class="form-control">
                    <input type="submit" class="btn" value="Зареєструватися" id="register">
                    
                </form>
                	<div class="regsuccess">
                		Реєстрація успішна
                	</div>
                	<div class="edrpo">
                    	<input type="text" class="form-control" placeholder="EDRPO" id="edrpo">
                    	
                    	<input type="button" class="btn" value="Yes" id="postreg">
                    </div>
                	
                <?=(isset($accountid))?'<div>'.$accountid.'</div>':''?>
                <?=(isset($codekyc))?'<div>'.$codekyc.'</div>':''?>
                <span>Вже з нами?</span>
                <a href="#" class="click-here">Увійти</a>
            </div>
        </div>
    </main>
</div>


<style>
	.edrpo, .regsuccess {
		display:none;
	}
</style>