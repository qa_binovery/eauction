<div id="container" class="clearfix">
    <div id="nav-bar">
        <a href="/lot/create" class="create-new-lot">Створити новий лот <i>+</i></a>
        <ul>
            <li><a href="#" class="active">Мої активні лоти</a></li>
            <li><a href="#">Мої готові лоти</a></li>
            <li><a href="#">Глобальний список лотів</a></li>
        </ul>
    </div>
    
    <main id="main-active-lots" class="main">
    	<pre><?php //print_r($lots) ?></pre>
    	
        <div id="filters-holder">
            <div class="filters-row">
                <form action="#">
                    <div class="check-box-block view">
                        <label class="checkbox active">
                            <input type="checkbox">
                            <span class="lbl padding-8"></span>
                            <i>Перегляд каталогу</i>
                        </label>
                        <label class="checkbox">
                            <input type="checkbox">
                            <span class="lbl padding-8"></span>
                            <i>Перегляд карти</i>
                        </label>
                    </div>
                    <div class="item-filter filter1">
                        <input type="text" placeholder="ID Лоту">
                        <select class="selectpicker">
                            <option>Умови договору</option>
                            <option>Умови договору 2</option>
                            <option>Умови договору 3</option> 
                        </select>
                    </div>
                    <div class="item-filter">
                        <select class="selectpicker">
                            <option>Район</option>
                            <option>Район 2</option>
                            <option>Район 3</option> 
                        </select>
                        <select class="selectpicker">
                            <option>Вид спеціального призначення</option>
                            <option>Вид 2</option>
                            <option>Вид 3</option> 
                        </select>
                    </div>
                    <div class="item-filter">
                        <select class="selectpicker">
                            <option>Регіон</option>
                            <option>Регіон 2</option>
                            <option>Регіон 3</option> 
                        </select>
                        <select class="selectpicker">
                            <option>Статус лоту</option>
                            <option>Статус 2</option>
                            <option>Статус 3</option> 
                        </select>
                    </div>
                    <div class="button-holder">
                        <input type="submit" value="Застосовувати">
                    </div>
                </form>
            </div>
        </div>
        <div class="lots-list-holder">
            <ul id="lots-list">
            	
            </ul>
            
            <div id="loading_lots"></div>
        </div>
    </main>
</div>
