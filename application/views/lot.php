<div id="container" class="clearfix">
    <div id="nav-bar" class="part_nav-bar">
        <ul>
            <li><a href="#">Глобальний список лотів</a></li>
            <li><a href="#" class="active">Мої активні лоти</a></li>
            <li><a href="#">Історія лотів</a></li>
        </ul>
    </div>
    <input type="hidden" id="lotid" value="<?=$lot?>" />
    <main id="lot-description" class="main">
        <div class="title-line"></div>
        <div class="content-description">
            <div class="map-brief-holder clearfix">
                <div class="map-box" id="map" style="padding:0;">
                	
                	
                	<iframe width="100%" height="100%" src="http://test.svcontact.ru/map.php"></iframe>
                	
                </div>
                <div class="brief-description">
                    <h2>&laquo;Українська виробничо-наукова лабораторія імуногенетики&raquo;</h2>
                    <p class="address">Ідентифікаційний код 12345678<br>, 07400, Київська обл., м. Київ, вул. Грушевського, 100.<br> Відокремлені підрозділі та предствавництва відсутні.</p>
                </div>
            </div>
            <div class="description-info">
                <div class="tabs-btn">
                    <button type="button" class="descript-btn active">Описання</button>
                    <button type="button" class="specif-btn">Специфікація</button>
                </div>
                <div class="text-block description-text active">
                    <h2>&laquo;Українська виробничо-наукова лабораторія імуногенетики&raquo;</h2>
                    <p class="address">Ідентифікаційний код 12345678<br>, 07400, Київська обл., м. Київ, вул. Грушевського, 100.<br> Відокремлені підрозділі та предствавництва відсутні.</p>
                    <p class="plcond">Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. "Риба" не тільки успішно пережила п'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп'ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>
                </div>
                <div class="text-block specification-text">
                    <p class="spec">Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. "Риба" не тільки успішно пережила п'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп'ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.</p>
                </div>
                
                <div class="bids">
                	<div class="startprice">Початкова ціна: <span></span></div>
                	<div class="minstep">Найменший можливий крок: <span></span></div>
                	<div class="maxstep">Найбільший можливий крок: <span></span></div>
                	<div class="publicationtime">Створено: <span></span></div>
                	
                </div>
                
                <a href="#" class="participate">Взяти участь</a>
            </div>
        </div>
    </main>
</div>
